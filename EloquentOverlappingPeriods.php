<?php

Mymodel::where(function ($query) use ($start_date, $end_date) {
    return $query->where(function ($query) use ($start_date, $end_date) {
        if ($end_date) {
            return $query->where('start_date', '<=', $start_date)->where('end_date', '<', $end_date)->where('end_date', '>', $start_date);
        } else {
            return $query->where('start_date', '<=', $start_date)->where('end_date', '>', $start_date);
        }
    })
        ->orWhere(function ($query) use ($start_date, $end_date) {
            if ($end_date) {
                return $query->where('start_date', '>', $start_date)->where('end_date', '>=', $end_date)->where('start_date', '<', $end_date);
            } else {
                return $query->where('start_date', '>', $start_date);
            }
        })
        ->orWhere(function ($query) use ($start_date, $end_date) {
            if ($end_date) {
                return $query->whereStartDate($start_date)->whereEndDate($end_date);
            } else {
                return $query->whereStartDate($start_date)->whereNull('end_date');
            }
        })
        ->orWhere(function ($query) use ($start_date, $end_date) {
            if ($end_date) {
                return $query->where('start_date', '<', $start_date)->where('end_date', '>', $end_date);
            } else {
                return $query->where('start_date', '<', $start_date)->whereNull('end_date');
            }
        });
});
