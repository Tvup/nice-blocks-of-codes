#!/bin/bash
sudo add-apt-repository universe
sudo apt-get update
sudo apt-get install build-essential libxml2-dev libcurl4-openssl-dev pkg-config libbz2-dev libicu-dev libreadline-dev apache2-dev libmcrypt-dev libmcrypt4 libxslt1.1 libxslt1-dev php5 -y
curl -L -O https://github.com/phpbrew/phpbrew/raw/master/phpbrew
chmod +x phpbrew
sudo mv phpbrew /usr/local/bin/phpbrew
mkdir ~/.phpbrew
phpbrew known --old
sudo chmod -R oga+rw /usr/lib/apache2/modules
sudo chmod -R oga+rw /etc/apache2
wget https://mail.gnome.org/archives/xml/2012-August/txtbgxGXAvz4N.txt
phpbrew install --patch txtbgxGXAvz4N.txt php-5.3.10 +default+apxs2+mcrypt+mysql
phpbrew init
echo "source /home/ubuntu/.phpbrew/bashrc" | sudo tee -a ~/.bashrc
source /home/ubuntu/.phpbrew/bashrc
phpbrew use php-5.3.10
phpbrew switch php-5.3.10
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
composer create-project laravel/laravel dev 4.1.*
sudo cp -R ./dev/ /var/www/html/

sudo chgrp -R www-data /var/www/html/dev
sudo chmod -R 775 /var/www/html/dev/app/storage
echo "<VirtualHost *:80>
    ServerName yourdomain.tld

    ServerAdmin webmaster@localhost
    DocumentRoot /var/www/html/dev/public

    <Directory /var/www/html/dev>
        AllowOverride All
    </Directory>

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>" | sudo tee -a /etc/apache2/sites-available/laravel.conf

sudo a2dissite 000-default.conf
sudo a2ensite laravel.conf
sudo a2enmod rewrite
sudo service apache2 restart